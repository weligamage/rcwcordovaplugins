package com.rcw.plugs.car;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Car extends CordovaPlugin {

	public Car() {
	}

	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

		if (action.equals("drive")) {
			this.drive(args.getInt(0), args.getString(1));
			return true;
		} else if (action.equals("break")) {
			this.breakNow(callbackContext);
			// callbackContext.success();
			return true;

		} else {
			return false;
		}
	}

	public void drive(int gear, String driveMode) {
		if (driveMode.equals("slow")) {
			Log.d("Car", "driving slowly.....");
		} else {
			Log.d("Car", "driving -->" + driveMode);
		}
	}

	public void breakNow(final CallbackContext callbackContext) {
		final JSONObject result = new JSONObject();
		try {
			result.put("deviceTime", System.currentTimeMillis());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
	}
}